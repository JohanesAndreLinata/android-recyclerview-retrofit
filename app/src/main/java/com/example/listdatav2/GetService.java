package com.example.listdatav2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetService {
    @GET("/api/users")
    Call<Data> getAllUsers();
}

