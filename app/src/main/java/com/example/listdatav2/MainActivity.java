package com.example.listdatav2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;


import com.example.listdatav2.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private ListDataAdapter adapter;
    private ActivityMainBinding binding;
    ProgressDialog progressDialog;
    MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        initAdapter();
//        mainViewModel = new MainViewModel();
        mainViewModel.requestUser();
        mainViewModel.users.observe(this, new Observer<Data>() {
            @Override
            public void onChanged(Data dataList) {
                if(dataList != null){
//                    generateDataList(new ArrayList<ListData>());
                updateDataList(dataList.getData());
                progressDialog.hide();
//                    Log.d("Johanes", "onChanged: " + dataList.getData().size() ); untuk debug
                }
            }
        });
    }


    private void initAdapter(){
        adapter = new ListDataAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
    }

    private void updateDataList(List<ListData> dataList) {
       adapter.updateData(dataList);
    }

}