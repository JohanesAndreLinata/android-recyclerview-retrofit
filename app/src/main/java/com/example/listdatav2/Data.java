package com.example.listdatav2;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
//    @SerializedName("data")
    public List<ListData> data;

    public List<ListData> getData() {
        return data;
    }

    public void setData(List<ListData> data) {
        this.data = data;
    }
    public Integer page;
}
