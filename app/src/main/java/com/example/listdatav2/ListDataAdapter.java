package com.example.listdatav2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.listdatav2.databinding.ListDataBinding;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ListDataAdapter extends RecyclerView.Adapter<ListDataAdapter.ListDataViewHolder> {

    private List<ListData> dataList = new ArrayList<>();

    class ListDataViewHolder extends RecyclerView.ViewHolder{


        private ListDataBinding binding;

        ListDataViewHolder(ListDataBinding binding){
            super(binding.getRoot());

            this.binding = binding;
        }
    }
    @Override
    public ListDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListDataBinding view = ListDataBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);

        ListDataViewHolder holder = new ListDataViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ListDataViewHolder holder, int position) {
        holder.binding.setUser(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void updateData(List<ListData> newList){
        dataList.clear();
        dataList.addAll(newList);
        notifyDataSetChanged();
    }
}
