package com.example.listdatav2;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {
    public MutableLiveData<Data> users = new MutableLiveData<>();

    public void requestUser() {
        GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<Data> call = service.getAllUsers();
        call.enqueue(new Callback<Data>() {


            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                users.postValue(response.body());
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                users.postValue(null);
            }
        });
    }
}
